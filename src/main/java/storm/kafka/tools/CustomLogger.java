package storm.kafka.tools;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Properties;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;

public class CustomLogger {
	Logger realTimeLogger;

	public CustomLogger(String name){
			realTimeLogger = Logger.getLogger(name);
		}
	public  void log(String msg){
		try{

		realTimeLogger.info(msg);
		}catch(Exception e){
			e.printStackTrace();
		}
	
	  }
}